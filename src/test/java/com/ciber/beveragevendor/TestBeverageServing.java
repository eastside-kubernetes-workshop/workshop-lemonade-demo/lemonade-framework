package com.ciber.beveragevendor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

class TestBeverageServing {

    @Test
    void testBeverageServing() {
        BeverageServing serving = new BeverageServing();
        assertNotNull(serving);
    }

    @Test
    void testBeverageServingEmpty() {
        BeverageServing serving = new BeverageServing();
        assertNotNull(serving);
        assertNotNull(serving.getServingId());
        assertIsNull(serving.getBeverageType());
        assertEquals(0, serving.getAmount());
        assertEquals(0, serving.getCostCents());
    }

    private void assertIsNull(String beverageType) {
        // TODO Auto-generated method stub

    }

    @Test
    void testBeverageServingStringInt() {
        BeverageServing serving = new BeverageServing("lemonade", 10);
        assertNotNull(serving);
        assertNotNull(serving.getServingId());
        assertEquals("lemonade", serving.getBeverageType());
        assertEquals(10, serving.getAmount());
        assertEquals(5, serving.getCostCents());
    }

}
