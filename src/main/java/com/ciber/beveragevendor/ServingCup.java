package com.ciber.beveragevendor;

import java.util.UUID;

import lombok.Data;

/**
 * Represents a cup which can hold a serving for a beverage
 */
@Data
public class ServingCup {
    String cupId;
    final String product = "cup";
    final String units = "size";
    String size;
    int ounces;
    int costCents;

    public ServingCup() {

    }

    public ServingCup(String size) {
        UUID uuid = UUID.randomUUID();
        this.cupId = uuid.toString();
        this.costCents = 10;
        this.size = size;
        if (size.equalsIgnoreCase("large")) {
            ounces = 16;
            return;
        }
        if (size.equalsIgnoreCase("medium")) {
            ounces = 12;
            return;
        }
        if (size.equalsIgnoreCase("small")) {
            ounces = 8;
            return;
        }
        // default
        throw new IllegalArgumentException("Ivalid size: " + size);
    }

}
